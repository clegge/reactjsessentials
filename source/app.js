var React = require('react');
var ReactDOM = require('react-dom');

/*

var h1 = React.createElement('h1', {className: 'header', key: 'header'}, 'This is React');
var p = React.createElement('p', {className: 'content', key: 'content'}, 'And thats how it works');
var reactFragment = [h1, p];
var section = React.createElement('section', {className: 'container'}, reactFragment);

ReactDOM.render(section, document.getElementById('react-application'));
*/

/*

var createListItemElement = React.createFactory('li');
var li1 = createListItemElement({className: 'item-1', key: 'item-1',}, 'item 1');
var li2 = createListItemElement({className: 'item-2', key: 'item-2',}, 'item 2');
var li3 = createListItemElement({className: 'item-3', key: 'item-3',}, 'item 3');

var reactFragment2 = [li1, li2, li3];
var listOfItems = React.createElement('ul', {className:'list-of-items'}, reactFragment2);
ReactDOM.render(listOfItems, document.getElementById('react-application'));

*/


/*
var li1 = React.DOM.li({className: 'item-1', key: 'item-1'}, 'item 1');
var li2 = React.DOM.li({className: 'item-2', key: 'item-2'}, 'item 2');
var li3 = React.DOM.li({className: 'item-3', key: 'item-3'}, 'item 3');

var reactFragment2 = [li1, li2, li3];
var listOfItems = React.DOM.ul({className: 'list-of-items'}, reactFragment2);
ReactDOM.render(listOfItems, document.getElementById('react-application'));
*/

var listOfItems = <ul className="lilst-of-items">
                        <li className="item-1">Item 1</li>
                        <li className="item-2">Item 2</li>
                        <li className="item-3">Item 3</li>
                  </ul>;


ReactDOM.render(listOfItems, document.getElementById('react-application'));

